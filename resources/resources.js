'use strict';

import {devices} from './device-list.js';

const date = new Date();
const currentHour = date.getHours();

function getValues(array, time) {
  const currentArray = array.filter((array) => {
    return array.hour === `${time}-${time + 1}`;
  });

  const highValues = array.reduce((high, currentObject) => {
    if (currentObject.price > high.price) {
      high = currentObject;
    }
    return high;
  });
  const lowValues = array.reduce((low, currentObject) => {
    if (currentObject.price < low.price) {
      low = currentObject;
    }
    return low;
  });

  const currentValues = currentArray[0];

  return {
    currentValues,
    highValues,
    lowValues,
  };
}

async function getAPIData(APIurl){
  const response = await fetch(APIurl);
  const body = await response.json();
  const array = Object.values(JSON.parse(body.contents));
  return getValues(array, currentHour);
}

function getPrice(source, device) {
  return `${source.hour} horas / ${
    Math.round((device.watts / 100) * source.price) / 10000
  } €/h`;
}

function createLi(device, data, destination){
  const li = document.createElement('li');
  li.innerHTML = `
  <h2>${device.name.toLocaleUpperCase()}</h2>
      <img src="./images/${device.name}.jpg" alt="${device.name}" />
      <p class="static">Consumo actual</p>
      <p class="current">${getPrice(data.currentValues, device)}</p>
      <p class="static">Franja horaria más cara</p>
      <p class="high">${getPrice(data.highValues, device)}</p>
      <p class="static">Franja horaria más barata</p>
      <p class="low">${getPrice(data.lowValues, device)}</p>
  `;
  destination.append(li);
}

function APIDataToUl(data) {
  const devicesList = document.querySelector('ul');
  const frag = document.createDocumentFragment();
  for (let index = 0; index < devices.length; index++) {
    createLi(devices[index], data, frag);
  }
  devicesList.append(frag);
}

export {date, APIDataToUl, getAPIData};