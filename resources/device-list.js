const devices = [
    {
      name: 'lavadora',
      watts: 1850,
    },
    {
      name: 'tostadora',
      watts: 775,
    },
    
    {
      name: 'calefacción',
      watts: 1750,
    },
  
    {
      name: 'aire acondicionado',
      watts: 1450,
    },
    
    {
      name: 'frigorífico',
      watts: 300,
    },
    
    {
      name: 'televisión',
      watts: 275,
    },
    
    {
      name: 'microondas',
      watts: 825,
    },
    
    {
      name: 'horno',
      watts: 1700,
    },
    
    {
      name: 'vitrocerámica',
      watts: 1450,
    },
    
    {
      name: 'lavavajillas',
      watts: 1850,
    },
  
    {
      name: 'aspiradora',
      watts: 1200,
    },
  ];

  export {devices}