'use strict';
import {
  date,
  APIDataToUl,
  getAPIData
} from './resources/resources.js';

const update = async (
  url = 'https://api.allorigins.win/get?url=https://api.preciodelaluz.org/v1/prices/all?zone=PCB'
) => {
  try {
    const APIData = await getAPIData(url);
    APIDataToUl(APIData);
    APIData.lastFetch = date.getTime();
    localStorage.setItem('APIData', JSON.stringify(APIData));
  } catch (err) {
    console.error(err);
  }
};

const APIData = JSON.parse(localStorage.getItem('APIData'));
const fiveMinInMs = 300000;

if (!APIData || date.getTime() - APIData.lastFetch >= fiveMinInMs) {
  update();
} else {
  APIDataToUl(APIData);
}